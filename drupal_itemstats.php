<?php
include_once(dirname(__FILE__) . '/itemstats.php');

function itemstats_parse($message)
{
  $item_stats = new ItemStats();
  if ($item_stats->connected == false)
    return ($message);

  //Detect itememb tags next to each other and put put them into a table 
  while (preg_match('#((\[itememb(=[0-5])?\][^\[]*\[/itememb\]){2,})#s', $message, $match)) 
  {
    $table_html = '<table style="margin: 0; border-width: 0; width: 0;"><tbody style="border-top: 0px none #F00;"><tr>' . $match[1] . '</tr></tbody></table>';
    while (preg_match('#(\[itememb(=[0-5])?\][^\[]*\[/itememb\])(?!\</td\>)#s', $table_html, $match2))
     {
      $item_html = '<td style=" vertical-align: top; padding: 0px;">' . $match2[1] . '</td>';
      $table_html = str_replace($match2[0], $item_html, $table_html);
    }
    $message = str_replace($match[0], $table_html, $message);
  }

  // Search for [item] tags, and replace them with HTML for the specified item.
  while (preg_match('#\[(item)(=[0-5])?\](.+?)\[/item\]#s', $message, $match) OR preg_match('#\[(itemico)(=[0-5])?\](.+?)\[/itemico\]#s', $message, $match) OR preg_match('#\[(itememb)(=[0-5])?\](.+?)\[/itememb\]#s', $message, $match) OR preg_match('#\[(itembi)(=[0-5])?\](.+?)\[/itembi\]#s', $message, $match))
  {
    // Grab the item name.
    $item_name = $match[3];
    $icon_lsize = $match[2];
    $item_type = $match[1];

    //===   Previous processing   =========================================
    // On transforme les entites HTML en vrai caract :)
    $item_name = html_entity_decode($item_name, ENT_QUOTES);
    //=====================================================================

    //===   GET HTML FOR DISPLAY   ========================================
    if ($item_type=='itememb')
      $item_html = $item_stats->getItemForDisplay($item_name, 'item', $icon_lsize, automatic_search);
    else if ($item_type=='itembi')
      $item_html = $item_stats->getItemForDisplay($item_name, 'itemico', '=1', automatic_search);
    else
      $item_html = $item_stats->getItemForDisplay($item_name, $item_type, $icon_lsize, automatic_search);
    //=====================================================================

    //===    Next processing   ============================================
    $item_html = str_replace("{PATH_ITEMSTATS}", path_itemstats, $item_html);
    $item_html = str_replace("8)", "8 )", $item_html);
    //=====================================================================



    //Last minute changes for Drupal
    if ($item_type == 'itemico' or $item_type=='itememb' or $item_type=='itembi')
      $item_html = preg_replace("/<td valign=\\\'top\\\' class=\\\'itemicon\\\'>[ \t\n\r\f\v]*?<img class=\\\'itemicon\\\' src=\\\'.*?\\\'>[ \t\n\r\f\v]*?<\/td>/", "", $item_html);
    if ($item_type=='itememb')
    {
      $item_html = preg_replace("/<a class='forumitemlink'.*?href='(.*?)'>.*?<span onmouseover=\"return overlib\(\'.+?(<div class=\\\'wowitem.*?<span class=\\\'(redname|orangename|purplename|bluename|greenname|whitename|greyname)\\\'>)(.*?)(<\/span>.*)<\/td>.*?<\/tr>.*?<\/table>.*/",
                                "\\2<a target='_blank' href='\\1'>\\4</a>\\5", $item_html);
      $item_html = preg_replace("/<a class='forumitemlink'.*?href='(.*?)'>.*?<span onmouseover=\"return overlib\(\'.+?<table class=\\\'wowitemt\\\'>.*?<td>(.*?)<\/td>.*?<\/tr>.*?<\/table>.*/", 
                                "<div class='errorname wowitem'><a href='\\1'>\\2</a></div>", $item_html);
	$item_html = str_replace("\'", "'", $item_html);
    }
    else if ($item_type=='itembi'){
      $item_html = preg_replace("#(^.*<span class=\\\'(redname|orangename|purplename|bluename|greenname|whitename|greyname)\\\'>.*)</span>.*?<\/a>#", "\\1<span class='\\2'>[" . $item_name . "]</span></span></a>", $item_html);
      $item_html = preg_replace("#(^.*<table class=\\\'wowitemt\\\'>.*).*?</span>.*?<\/a>#", "\\1<span class='greyname'>[" . $item_name . "]</span></span></a>", $item_html);
    }
      //Hack out direct update.php url call
      $item_html = preg_replace("/href='.*?updateitem.php\?item=(.*?)'>/", "href='" . url('warcraft-itemstats/update', 'item=\\1' . '&destination=' .  $_GET['q']). "'>", $item_html);
    // Finally, replace the bbcode with the html.
    $message = str_replace($match[0], $item_html, $message);
  }

    return $message;
}

function itemstats_parse_one_item($item, $itemstats_path)
{
	$item_stats = new ItemStats();
	if ($item_stats->connected == false)
        return ($item);

	// Grab the item name.
	$item_name = $item;
    $item_type = 'item'; // For the moment, we put only text for these objects
    $icon_lsize = 0;

    //===   GET HTML FOR DISPLAY   ========================================
    $item_html = $item_stats->getItemForDisplay($item_name, $item_type, $icon_lsize, true);
    //=====================================================================

    //===    Next processing   ============================================
    $item_html = str_replace("{PATH_ITEMSTATS}", $itemstats_path, $item_html);
    //=====================================================================
      //Hack out direct update.php url call
      $item_html = preg_replace("/href='.*?updateitem.php\?item=(.*?)'>/", "href='" . url('warcraft-itemstats/update', 'item=\\1' . '&destination=' .  $_GET['q']). "'>", $item_html);

	return $item_html;
}


?>