TO install:
1) Install Itemstats ( http://itemstats.free.fr/news.php?lang=en-US )
2) Copy 'drupal_itemstats.php' into the root of your 'itemstats/' install directory.
3) Unzip the folder 'modules' into the drupal base directory.
4) Go to 'Home � Administer � Site building � Modules' to activate 'Warcraft Itemstats' under the 'Optional' sub-section.
5) Go to 'Home � Administer � Site configuration � Warcraft Itemstats settings' and enter path information. 
6) Go to 'Home � Administer � Site configuration � Input formats' and 'configure' each input format to include the Itemstat filter.
7) Make a new test post to ensure everything is functioning as intended.